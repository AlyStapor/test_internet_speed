
import speedtest

test = speedtest.Speedtest()

option = int(input('''What speed to you want to test? : 
    1) Download Speed 
    2) Upload Speed 
    3) Ping   
    Your option:  
    '''))

if option ==1:
    print(f"Your Download Speed is {test.download()} bytes / second ")

elif option==2:
    print(f" Your Upload Speed is {test.upload()} bytes / second ")

elif option==3:
    second_option = int(input('''What you want to check? : 
        1) Available servers
        2) Best server
        3) Closest server 
        4) Ping   
        Your option:  
        '''))

    if second_option==1:
        available_servers = []
        print(f"Available servers: {test.get_servers(available_servers)}")

    elif second_option==2:
        best_server = test.get_best_server()

        for key, value in best_server.items():
            print(key, " : ", value)

    elif second_option==3:
        closest_server = test.get_closest_servers()

        for key, value in closest_server[1].items():
            print(key, " : ", value)

    elif second_option==4:
        print(f"Your Ping {test.results.ping}")

else:
    print("Please choose a correct option")